package com.lyq.blog.model;

import com.lyq.blog.constants.ResultCodeEnum;
import lombok.Data;

@Data
public class Result<T> {
    private ResultCodeEnum code;
    private String errorInfo;
    private T data;

    public Integer getCode() {
        return code.getCode();
    }

    /**
     * 成功响应
     * @param data 响应数据
     * @return 返回封装后的响应数据
     * @param <T>
     */
    public static<T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.code = ResultCodeEnum.SUCCESS;
        result.data = data;
        return result;
    }

    /**
     * 内部出现异常
     * @param errorInfo 异常信息
     * @return 返回封装后的数据
     * @param <T>
     */
    public static<T> Result<T> fail(String errorInfo) {
        Result<T> result = new Result<>();
        result.code = ResultCodeEnum.FAIL;
        result.errorInfo = errorInfo;
        return result;
    }

    /**
     * 用户未登录
     * @return 返回封装后的数据
     * @param <T>
     */
    public static<T> Result<T> loginOut() {
        Result<T> result = new Result<>();
        result.code = ResultCodeEnum.LOG_OUT;
        return result;
    }

}
