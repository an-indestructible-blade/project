package com.lyq.blog.mapper;

import com.lyq.blog.model.BlogInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BlogInfoMapper {
    /**
     * 插入新文章
     * @param blogInfo 文章信息
     * @return 插入成功返回 1
     */
    @Insert("insert into blog(title,content,user_id) values (#{title},#{content},#{userId});")
    Integer creatBlog(BlogInfo blogInfo);

    /**
     * 获取博客列表
     * @return 返回博客列表
     */
    @Select("select * from blog where delete_flag=1")
    List<BlogInfo> getList();
    /**
     * 根据博客id获取博客信息
     */
    @Select("select * from blog where delete_flag=1 and id=#{blogId}")
    BlogInfo getBlogById(Integer blogId);

    /**
     * 修改博客信息
     * @param blogInfo
     * @return
     */
    int updataBlog(BlogInfo blogInfo);
}
