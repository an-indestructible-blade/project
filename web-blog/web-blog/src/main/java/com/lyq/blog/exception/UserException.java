package com.lyq.blog.exception;

public class UserException extends RuntimeException {
    public UserException(String errorInfo) {
        super(errorInfo);
    }
}
