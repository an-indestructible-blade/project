package com.lyq.blog.config;

import com.lyq.blog.exception.UserException;
import com.lyq.blog.model.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
@ControllerAdvice
public class ErrorConfig {
    @ExceptionHandler
    public Result exception(Exception e) {
        e.printStackTrace();
        return Result.fail("内部发生异常");
    }
    @ExceptionHandler
    public Result exception(UserException e) {
        return Result.fail(e.getMessage());
    }
}
