package com.lyq.blog.controller;

import com.lyq.blog.exception.UserException;
import com.lyq.blog.model.Result;
import com.lyq.blog.model.UserInfo;
import com.lyq.blog.service.UserService;
import com.lyq.blog.utils.CryptographUtil;
import com.lyq.blog.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    /**
     * 根据博客id获取作者信息
     */
    @RequestMapping("getUserInfoByBlog")
    public Object getUserInfoByBlog(Integer blogId) {
        log.info("getUserInfoByBlog  blogId:{}", blogId);
        if (blogId == null || blogId <= 0) {
            return Result.fail("非法id");
        }
        return userService.getUserInfoByBlogId(blogId);
    }
    /**
     * 根据token获取用户信息
     */
    @RequestMapping("getUserInfo")
    public UserInfo getUserInfo(HttpServletRequest request) {
        String userToken = request.getHeader("user_token");
        log.info("getUserInfo 根据token获取用户信息 userToken:{}", userToken);
        //从token中获取用户id
        Claims claims = JwtUtil.parserToken(userToken);
        Integer id = (Integer)claims.get("id");
        return userService.getUserInfoById(id);
    }

    /**
     * 用户登录
     * @param userName 登录账号
     * @param password 登录密码
     * @return 登录成功返回Token，失败返回空字符串
     */
    @RequestMapping("/login")
    public Result<String> login(String userName, String password) {
        log.info("登录 userName:{} password:{}", userName, password);
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)) {
            throw new UserException("用户名或密码非法");
        }
        UserInfo userInfo = userService.getUserInfoByName(userName);
        if (userInfo != null && CryptographUtil.checkPassword(password, userInfo.getPassword())) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", userInfo.getId());
            map.put("userName", userInfo.getUserName());
            return Result.success(JwtUtil.creatToken(map));
        }
        log.info("登录失败");
        return Result.success(null);
    }

    /**
     * 注册
     */
    @RequestMapping("/createUser")
    public Result<String> creatUser(String userName, String password) {
        log.info("注册 userName:{} password:{}", userName, password);
        if (!StringUtils.hasLength(userName) || !StringUtils.hasLength(password)) {
            throw new UserException("用户名或密码非法");
        }
        UserInfo userInfo = null;
        String cryptograph = CryptographUtil.getCryptograph(password);
        userInfo = userService.creatUser(userName,cryptograph);
        if (userInfo == null) {
            log.info("账号已存在");
            throw new UserException("改账号已存在，请重新输入账号");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("id", userInfo.getId());
        map.put("userName", userInfo.getUserName());
        log.info("账号创建成功 userName:{}", userInfo.getUserName());
        return Result.success(JwtUtil.creatToken(map));
    }
}
