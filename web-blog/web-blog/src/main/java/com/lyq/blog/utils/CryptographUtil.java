package com.lyq.blog.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Slf4j
public class CryptographUtil {
    /**
     * 对数据进行加密
     * @param password
     * @return
     */
    public static String getCryptograph(String password) {
        //密码+UUID
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String md5 = DigestUtils.md5DigestAsHex((password + uuid).getBytes());
        return md5+uuid;
    }
    /**
     * 验证原文和密文是否匹配
     */
    public static boolean checkPassword(String password ,String Cryptograph) {
        if (!StringUtils.hasLength(Cryptograph) || Cryptograph.length() < 64) {
            log.error("数据库密码异常");
            return false;
        }
        String uuid = Cryptograph.substring(32, 64);
        System.out.println(uuid);
        String tmp = DigestUtils.md5DigestAsHex((password + uuid).getBytes());
        return Cryptograph.substring(0, 32).equals(tmp);
    }
}
