package com.lyq.blog.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static String transformDate(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return simpleDateFormat.format(date);
    }
}
