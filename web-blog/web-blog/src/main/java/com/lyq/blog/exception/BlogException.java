package com.lyq.blog.exception;

public class BlogException extends RuntimeException{
    public BlogException(String str) {
        super(str);
    }
}
