package com.lyq.blog.model;

import com.lyq.blog.utils.DateUtil;
import lombok.Data;

import java.util.Date;

@Data
public class BlogInfo {
    private Integer id;
    private String title;
    private String content;
    private Integer userId;
    private Integer deleteFlag; //0:删除  1:正常
    private Date createTime;
    private Date updateTime;
    private Boolean isLoginUser;

    public String getCreateTime() {
        return DateUtil.transformDate(this.createTime);
    }

    public void setUpdateTime(Date date) {
        this.updateTime = date;
    }
}
