package com.lyq.blog.model;

import com.lyq.blog.utils.DateUtil;
import lombok.Data;

import java.util.Date;

@Data
public class UserInfo {
    private Integer id;
    private String userName;
    private String password;
    private String githubUrl;
    private Integer deleteFlag; //0:删除  1:正常
    private Date createTime;
    private Date updateTime;

    public String getCreateTime() {
        return DateUtil.transformDate(this.createTime);
    }

    public String getUpdateTime() {
        return DateUtil.transformDate(this.createTime);
    }
}
