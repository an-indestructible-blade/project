package com.lyq.blog.service;

import com.lyq.blog.exception.UserException;
import com.lyq.blog.mapper.BlogInfoMapper;
import com.lyq.blog.mapper.UserInfoMapper;
import com.lyq.blog.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private BlogInfoMapper blogInfoMapper;
    /**
     * 根据用户id获取用户信息
     * @return 用户信息
     */
    public UserInfo getUserInfoByBlogId(Integer id) {
        //获取用户id
        Integer userId = blogInfoMapper.getBlogById(id).getUserId();
        UserInfo userInfo = userInfoMapper.getUserInfoById(userId);
        if (userInfo == null) {
            throw new UserException("该用户不存在");
        }
        return userInfo;
    }

    /**
     * 根据用户id获取用户信息
     * @param id 用户id
     * @return 用户信息
     */
    public UserInfo getUserInfoById(Integer id) {
        return userInfoMapper.getUserInfoById(id);
    }

    public UserInfo creatUser(String userName, String password) {
        if (userInfoMapper.getUserInfoByName(userName) != null) {
            return null;
        }
        if (1 == userInfoMapper.createUser(userName, password)) {
            return userInfoMapper.getUserInfoByName(userName);
        }
        return null;
    }

    public UserInfo getUserInfoByName(String userName) {
        return userInfoMapper.getUserInfoByName(userName);
    }
}
