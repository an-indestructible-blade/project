package com.lyq.blog.mapper;

import com.lyq.blog.model.UserInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserInfoMapper {
    /**
     * 根据id查找
     * @param id 用户id
     * @return 返回找到的用户id
     */
    @Select("select * from user where id=#{id} and delete_flag=1;")
    UserInfo getUserInfoById(Integer id);

    /**
     * 创建新用户
     * @return 操作成功返回 1
     */
    @Insert("insert into user(user_name, password) values (#{userName}, #{password});")
    Integer createUser(String userName, String password);

    /**
     * 根据用户名称获取用户信息
     */
    @Select("select * from user where user_name=#{userName} and delete_flag=1;")
    UserInfo getUserInfoByName(String userName);
}
