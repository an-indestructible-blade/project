package com.lyq.blog.constants;

import lombok.Getter;

public enum ResultCodeEnum {
    SUCCESS("success", 1),
    FAIL("fail", 0),
    LOG_OUT("logOut", -1);

    private String desc;
    @Getter
    private Integer code;

    ResultCodeEnum(String desc, int code) {
        this.desc = desc;
        this.code = code;
    }
}
