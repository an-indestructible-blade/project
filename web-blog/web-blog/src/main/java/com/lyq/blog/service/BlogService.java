package com.lyq.blog.service;

import com.lyq.blog.exception.BlogException;
import com.lyq.blog.mapper.BlogInfoMapper;
import com.lyq.blog.model.BlogInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogService {
    @Autowired
    private BlogInfoMapper blogInfoMapper;
    /**
     * 根据id获取博客
     */
    public BlogInfo getBlogById(Integer blogId) {
        BlogInfo blogInfo = blogInfoMapper.getBlogById(blogId);
        if (blogInfo == null) {
            throw new BlogException("该文章不存在");
        }
        return blogInfo;
    }

    public List<BlogInfo> getList() {
        return blogInfoMapper.getList();
    }

    /**
     * 添加博客
     * @param blogInfo
     * @return
     */
    public boolean addBlog(BlogInfo blogInfo) {
        if (blogInfoMapper.creatBlog(blogInfo) == 1) {
            return true;
        }
        return false;
    }

    /**
     * 修改博客信息
     * @param blogInfo
     * @return
     */
    public boolean updataBlog(BlogInfo blogInfo) {
        if (blogInfoMapper.updataBlog(blogInfo) == 1) {
            return true;
        }
        return false;
    }
}
