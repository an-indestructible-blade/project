package com.example.code;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;


@RestController
@RequestMapping("/login")
public class Check {
    public static final long MINUTES = 60*1000;

    @RequestMapping("/check")
    public boolean check(String str, HttpSession session) {
        if (!StringUtils.hasLength(str)) {
            return false;
        }
        if (((String)session.getAttribute("HOME_KAPTCHA_SESSION_KEY")).equalsIgnoreCase(str)
        && System.currentTimeMillis() - ((Date)session.getAttribute("HOME_KAPTCHA_SESSION_DATE")).getTime() < MINUTES) {
            return true;
        }
        return false;
    }
}
