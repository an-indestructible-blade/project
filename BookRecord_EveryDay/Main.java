import java.io.*;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<Book> bookList = new LinkedList<>();
        init(bookList);
        while(true){
            int choice;
            do{
                choice = menu();
            }while(choice < 0 || choice > 4);

            switch (choice) {
                case 1:
                    find(bookList);
                    break;
                case 2:
                    add(bookList);
                    break;
                case 3:
                    buy(bookList);
                    break;
                case 4:
                    show(bookList);
                    break;
                case 0:
                    exit(bookList);
                    break;
            }
        }
    }

    private static void buy(List<Book> bookList) {
        Scanner in = new Scanner(System.in);
        System.out.print("请输入图书编号：");
        int num = in.nextInt();
        System.out.print("请输入数目：");
        int count = in.nextInt();
        for (int i = 0; i < bookList.size(); i++) {
            if (num == bookList.get(i).getNum()){
                Book tmp = bookList.get(i);
                if (tmp.getCount() < count) {
                    System.out.println("库存不足");
                }else {
                    tmp.setCount(tmp.getCount()-count);
                    if (tmp.getCount() == 0) {
                        bookList.remove(i);
                        return;
                    }
                    tmp.setSum(tmp.getCount()*tmp.getMoney());
                }
                return;
            }
        }
        System.out.println("未收录该书");
    }

    private static void find(List<Book> bookList) {
        if (bookList.size() == 0) {
            System.out.println("未收录该书");
            return;
        }
        Scanner in = new Scanner(System.in);
        System.out.print("请输入图书编号：");
        int num = in.nextInt();
        for (Book book : bookList) {
            if (book.getNum() == num) {
                System.out.println(book);
                return;
            }
        }
        System.out.println("未收录该书");
    }

    private static void show(List<Book> bookList) {
        for(Book book : bookList) {
            System.out.println(book);
        }
    }

    private static void add(List<Book> bookList) {
        Scanner in = new Scanner(System.in);
        Book book= new Book();
        System.out.print("请输入图书编号：");
        book.setNum(in.nextInt());
        System.out.print("请输入书名：");
        book.setName(in.next());
        System.out.print("请输入出版社：");
        book.setAuthor(in.next());
        System.out.print("请输入单价：");
        book.setMoney(in.nextInt());
        System.out.print("请输入数目：");
        book.setCount(in.nextInt());
        book.setSum(book.getCount()*book.getMoney());

        int num = book.getNum();
        for (int i = 0; i < bookList.size(); i++) {
            if (num == bookList.get(i).getNum()){
                Book tmp = bookList.get(i);
                tmp.setCount(tmp.getCount()+book.getCount());
                tmp.setSum(tmp.getCount()*tmp.getMoney());
                return;
            }
        }
        bookList.add(book);
    }


    private static int menu() {
        System.out.println("###########################");
        System.out.println("1.查找图书");
        System.out.println("2.新增图书");
        System.out.println("3.购买图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("###########################");
        System.out.print("请选择你的操作->");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    private static void exit(List<Book> bookList) {
        Calendar calendar = Calendar.getInstance();
        String str = calendar.get(Calendar.YEAR)+"";
        str += calendar.get(Calendar.MONTH)+1;
        str += calendar.get(Calendar.DAY_OF_MONTH);
        try(OutputStream os = new FileOutputStream("销售记录"+str+".csv")){
            for (Book book : bookList) {
                String num = book.getNum()+" ";
                String name = book.getName()+" ";
                String author = book.getAuthor()+" ";
                String money = book.getMoney()+" ";
                String count = book.getCount()+" ";
                String sum = book.getSum()+" \n";
                os.write(num.getBytes());
                os.write(name.getBytes());
                os.write(author.getBytes());
                os.write(money.getBytes());
                os.write(count.getBytes());
                os.write(sum.getBytes());

                os.flush();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            System.exit(0);
        }
    }

    private static void init(List<Book> bookList) {
        Calendar calendar = Calendar.getInstance();
        String str = calendar.get(Calendar.YEAR)+"";
        str += calendar.get(Calendar.MONTH)+1;
        str += calendar.get(Calendar.DAY_OF_MONTH);
        try(InputStream is = new FileInputStream("销售记录"+str+".csv")) {
            Scanner in = new Scanner(is);
            while(in.hasNext()) {
                Book book = new Book();

                book.setNum(Integer.valueOf(in.next()));
                book.setName(in.next());
                book.setAuthor(in.next());
                book.setMoney(Double.valueOf(in.next()));
                book.setCount(Integer.valueOf(in.next()));
                book.setSum(Double.valueOf(in.next()));
                bookList.add(book);
            }
        } catch (FileNotFoundException e) {
            return;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

