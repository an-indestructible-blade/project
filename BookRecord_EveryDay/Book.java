public class Book {
    private int num;
    private String name;
    private String author;
    private double money;
    private int count;
    private double sum;

    public Book(){};

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "图书编号=" + num +
                ", 图书名称='" + name + '\'' +
                ", 出版社='" + author + '\'' +
                ", 单价=" + money +
                ", 库存数量=" + count +
                ", 总价=" + sum ;
    }
}
