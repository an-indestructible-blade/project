package org.example;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.*;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MainPage {
    private static WebDriver webDriver;

    @BeforeAll
    public static void init() {
        System.setProperty("webdriver.edge.driver","C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedgedriver.exe");
        webDriver = new EdgeDriver();
        webDriver.manage().window().maximize();
    }

    @AfterAll
    public static void delete() {
        webDriver.close();
    }

    //进入主界面
    @BeforeEach
    public void intoPage() throws InterruptedException {
        webDriver.get("http://8.130.98.211:8080/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#userName")).sendKeys("zhangsan");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("zhangsan");
        webDriver.findElement(By.cssSelector("#submit")).click();
        //等待页面跳转
        sleep(200);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    //测试建群
    @Order(1)
    @Test
    public void addGroupTest1() throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.tab > div.tab-friend")).click();
        sleep(200);
        webDriver.findElement(By.cssSelector("#friend-list > button")).click();
        sleep(500);
        webDriver.findElement(By.cssSelector("#\\31 ")).click();
        webDriver.findElement(By.cssSelector("#\\33 ")).click();
        webDriver.findElement(By.cssSelector("#\\34 ")).click();
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.message-show > button")).click();
        sleep(500);
        File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotAs, new File("D:\\三人群.png"));
    }

    @Order(2)
    @Test
    public void addGroupTest2() throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.tab > div.tab-friend")).click();
        sleep(200);
        webDriver.findElement(By.cssSelector("#friend-list > button")).click();
        sleep(500);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.message-show > button")).click();
        sleep(500);
        File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotAs, new File("D:\\无人.png"));
    }

    //测试添加好友
//    @Order(1)
//    @ParameterizedTest
//    @CsvSource({"lyq, 你好！", "zzz, 你好！"})
    public void addFriendTest1(String name, String message) throws InterruptedException {
        webDriver.findElement(By.cssSelector("#search-input")).clear();
        webDriver.findElement(By.cssSelector("#search-input")).sendKeys(name);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.search > button")).click();
        sleep(1500);
        webDriver.findElement(By.cssSelector("#add-user")).sendKeys(message);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.message-show > div > button")).click();
        sleep(500);
        webDriver.switchTo().alert().accept();
    }
//    @Order(2)
//    @ParameterizedTest
//    @CsvSource({"lyq, lyq, true", "zzz, zzz, false"})
    public void addFriendTest2(String name, String password, boolean is) throws InterruptedException, IOException {
        webDriver.get("http://8.130.98.211:8080/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#userName")).sendKeys(name);
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.findElement(By.cssSelector("#submit")).click();
        //等待页面跳转
        sleep(200);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        if (is) {
            webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.message-show > div > button.true")).click();
            sleep(500);
            webDriver.switchTo().alert().accept();
            sleep(500);
            webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.tab > div.tab-friend")).click();
            File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAs, new File("D:\\"+name+".png"));
        } else {
            webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.message-show > div > button.false")).click();
            sleep(500);
            webDriver.switchTo().alert().accept();
            sleep(500);
            webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.tab > div.tab-friend")).click();
            File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAs, new File("D:\\"+name+".png"));
        }
    }

    //测试用户搜索
//    @ParameterizedTest
//    @CsvSource({"lyq"})
    public void sourceTest(String name) throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("#search-input")).sendKeys(name);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.search > button")).click();
        sleep(1500);
        //截图
        File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotAs, new File("D:\\搜索.png"));
    }

    //测试收发消息
//    @ParameterizedTest
//    @CsvSource({"在干嘛？"})
    public void setMessageTest(String message) throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("#session-list > li:nth-child(2)")).click();
        sleep(1000);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > textarea")).sendKeys(message);
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.right > div.ctrl > button")).click();
        //截图保存结果
        File screenshotAs = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotAs, new File("D:\\发送方.png"));
        //登录李四的账号查看消息是否收到
        webDriver.get("http://8.130.98.211:8080/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#userName")).sendKeys("lisi");
        webDriver.findElement(By.cssSelector("#password")).sendKeys("lisi");
        webDriver.findElement(By.cssSelector("#submit")).click();
        //等待页面跳转
        sleep(1000);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#session-list > li:nth-child(1)")).click();
        sleep(1000);
        //截图保存结果
        File screenshotAs1 = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotAs1, new File("D:\\接收方.png"));
    }

    //测试修改用户头像
    //预期结果：用户头像变为修改后的图片
//    @ParameterizedTest
//    @ValueSource(strings = {"C:\\Users\\13900\\Pictures\\头像.png"})
    public void setUserPhotoTest(String photoPath) throws IOException, InterruptedException {
        //截图保存当前的头像
        File old = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(old, new File("D:\\修改前.png"));
        //进行修改操作
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.user")).click();
        //等待页面跳转
        sleep(500);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //上传文件
        webDriver.findElement(By.cssSelector("#fileUpdate")).sendKeys(photoPath);
        webDriver.findElement(By.cssSelector("#upload")).click();
        sleep(1000);
        webDriver.switchTo().alert().accept();
        sleep(500);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //截图保存修改后的头像
        File newFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(newFile, new File("D:\\修改后.png"));
    }

    //UI 用户个人信息修改
//    @Test
    public void test() throws InterruptedException, IOException {
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.user")).click();
        //等待页面跳转
        sleep(500);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //截图
        File file = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("D://个人主页.png"));
    }

//    @Test
    //测试前已存在用户lisi
    //预期结果：第一个是false紧接着十二个是true，后面除最后一个是true其余都是false;
    public void setUserNameTest() throws InterruptedException {
        String[] values = {
            "lisi","aaa","123","12asd","as","12","a2","qwertyuiopasdfgh","1234567890123456","123456789012345w",
            "zhangsande","1234567890","qwe1234567",
            "a","1","qweqwwwwwwwqqqqwee","12132234534363623464",
            "qweqwe7dhf7y76647yruh","@uhas()","asds asds","13213  32",
            "@12312334","asd12 ","asd12@","","@(){}","     ","a@",
            "w ","2 ","2@","@#","  ","qwertyuiopasdfg@","qwertyuiopasdfg@","qwertyuiopasdfg ",
            "123456789012345 ", "123456789012345@","12345678901234 w",
            "12 456789012345w","12345678901234$w","*&^%$#@!@#$%^&*&",
            "                ","12345678901234562","oqwertyuiopasdfgh","oqwertyuiopasdfg4",
            "qwe@werewq","qwertyuiop ","123456789 ","123456789@","12345678d ","q12345678@",
            "!@#$%^&*()","          ","zhangsan"
        };
        boolean[] result = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            //进入用户信息修改页面
            webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.user")).click();
            //等待页面跳转
            sleep(500);
            webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            //修改用户名
            webDriver.findElement(By.cssSelector("#userName")).clear();
            webDriver.findElement(By.cssSelector("#userName")).sendKeys(values[i]);
            webDriver.findElement(By.cssSelector("#upload")).click();
            sleep(1000);
            //获取弹窗信息
            Alert alert = webDriver.switchTo().alert();
            String text = alert.getText();
            alert.accept();
            //等待页面跳转
            sleep(500);
            result[i] = "信息修改成功".equals(text);
        }
        System.out.println(Arrays.toString(result));
    }

//    @ParameterizedTest
//    @CsvSource({"zhangsan, zhangsan"})
    //测试修改用户密码
    //预期结果：true
    public void setPasswordTest(String userName, String password) throws InterruptedException {
        //进入用户信息修改页面
        webDriver.findElement(By.cssSelector("body > div.client-container > div > div.left > div.user")).click();
        //等待页面跳转
        sleep(500);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //设置密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.findElement(By.cssSelector("#upload")).click();
        sleep(300);
        //关闭弹窗提示
        webDriver.switchTo().alert().accept();
        //重新登录
        webDriver.get("http://8.130.98.211:8080/login.html");
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        webDriver.findElement(By.cssSelector("#userName")).sendKeys(userName);
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        webDriver.findElement(By.cssSelector("#submit")).click();
        sleep(500);
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //获取页面url
        System.out.println("http://8.130.98.211:8080/client.html".equals(webDriver.getCurrentUrl()));
    }
}
