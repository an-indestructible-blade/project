package org.example;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.edge.EdgeDriver;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class TestLogin {
    @Test
    public void login() throws InterruptedException {
        String[][] value = {{"lisi", "lisi"},
                {"lisi", "123"},
                {"aaa", "lisi"},
                {"adfsa", "1212"}};
        //预期结果：true、false、false、false
        boolean[] result = new boolean[value.length];
        for (int i = 0; i < value.length; i++) {
            result[i] = testLogin(value[i][0], value[i][1]);
        }
        System.out.println(Arrays.toString(result));
    }

    @Test
    public void register() throws InterruptedException {
        String[] values = {
                "aaa","123","12asd","as","12","a2","qwertyuiopasdfgh","1234567890123456","123456789012345w",
                "zhangsande","1234567890","qwe1234567",
                "a","1","qweqwwwwwwwqqqqwee","12132234534363623464",
                "qweqwe7dhf7y76647yruh","@uhas()","asds asds","13213  32",
                "@12312334","asd12 ","asd12@","","@(){}","     ","a@",
                "w ","2 ","2@","@#","  ","qwertyuiopasdfg@","qwertyuiopasdfg@","qwertyuiopasdfg ",
                "123456789012345 ", "123456789012345@","12345678901234 w",
                "12 456789012345w","12345678901234$w","*&^%$#@!@#$%^&*&",
                "                ","12345678901234562","oqwertyuiopasdfgh","oqwertyuiopasdfg4",
                "qwe@werewq","qwertyuiop ","123456789 ","123456789@","12345678d ","q12345678@",
                "!@#$%^&*()","          "
        };
        //预期结果：前十二个是true，后面都是false;
        boolean[] result = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            int a = i;
            Thread thread = new Thread(()->{
                try {
                    result[a] = testRegister(values[a], "123");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
            thread.start();
            if (i % 3 == 2) {
                Thread.sleep(6000);
            }
        }
        Thread.sleep(10000);
        System.out.println(values.length+"个测试用例");
        System.out.println(Arrays.toString(result));
    }

    @BeforeAll
    public static void init() {
        System.setProperty("webdriver.edge.driver","C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedgedriver.exe");
    }

    //判断注册是否成功
    public boolean testRegister(String name, String password) throws InterruptedException {
        WebDriver webDriver = new EdgeDriver();
        webDriver.get("http://8.130.98.211:8080/login.html");
        //点击注册
        webDriver.findElement(By.cssSelector("#submit2")).click();
        webDriver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        //判断此时是否处于注册页面
        String logo = webDriver.findElement(By.cssSelector("body > div > div > h3")).getText();
        if (!"注册".equals(logo)) {
            throw new RuntimeException("未跳转到注册页面");
        }
        //输入账号
        webDriver.findElement(By.cssSelector("#userName")).sendKeys(name);
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        //进行注册操作
        webDriver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(500);
        //对结果进行判断，并返回结果
        Alert alert = webDriver.switchTo().alert();
        String result = alert.getText();
        //点击确认
        alert.accept();
        Thread.sleep(500);
        boolean flag = "注册成功".equals(result);
        Thread.sleep(300);
        //如果注册成功则判断页面是否跳转至登录界面
        if (flag) {
            logo = webDriver.findElement(By.cssSelector("body > div > div > h3")).getText();
            boolean ret = "登录".equals(logo);
            webDriver.close();
            return ret;
        }
        webDriver.close();
        return false;
    }

    //登录测试
    private boolean testLogin(String name, String password) throws InterruptedException {
        WebDriver webDriver = new EdgeDriver();
        webDriver.get("http://8.130.98.211:8080/login.html");
        //等待页面加载
        webDriver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        //检查当前是否处于登录页面
        String logo = webDriver.findElement(By.cssSelector("body > div > div > h3")).getText();
        if (!"登录".equals(logo)) {
            throw new RuntimeException("当前登录页面打开异常");
        }
        //输入账号
        webDriver.findElement(By.cssSelector("#userName")).sendKeys(name);
        //输入密码
        webDriver.findElement(By.cssSelector("#password")).sendKeys(password);
        //进行登录
        webDriver.findElement(By.cssSelector("#submit")).click();
        //先强制等待1秒，然后再隐式等待，防止页面还未跳转就执行后续代码
        Thread.sleep(1000);
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //获取新页面的url
        try{
            //如果未登录成功会弹窗警告，所以需要处理异常
            String url = webDriver.getCurrentUrl();
            //判断页面是否成功跳转
            return "http://8.130.98.211:8080/client.html".equals(url);
        } catch (UnhandledAlertException e) {
            return false;
        } finally {
            webDriver.close();
        }
    }
}