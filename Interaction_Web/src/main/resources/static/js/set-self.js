//添加首页，我的，的点击事件
function initTab() {
    let hand = document.querySelector('.set .set-1');
    let self = document.querySelector('.set .set-2');

    hand.onclick = function() {
        location.href = "client.html";
    }

    self.onclick = function() {
        location.href = "set-self.html";
    }
}
initTab();
//显示用户头像
function showPhoto() {
    $.ajax({
        url: "user/getUserByToken",
        type: "get",

        success: function(result) {
            if (result.code == 200 && result.data != null) {
                //显示头像
                let userDiv = document.querySelector(".main .content .photo");
                let str = '<p>头像：</p>'+
                    '<img src="'+result.data.photo+'" width="100px" height="100px" title="头像">'+
                    '<input type="file" id="fileUpdate" name="file">';
                userDiv.innerHTML = str;
                userDiv = document.querySelector(".main .content .name");
                userDiv.innerHTML = '<p>昵称：</p>'+
                '<input type="text" id="userName" size="20" placeholder="'+result.data.userName+'">'
            }
            else if (result.code == 401) {
                location.href = "login.html";
            }
            else {
                alert(result.error);
            }
        }
    });
}
showPhoto();

//保存用户信息
function save(){
    var form = new FormData();
    form.append('file', $('#fileUpdate')[0].files[0]);
    form.append('userToken', localStorage.getItem("userToken"));
    // 保存头像
    if ($('#fileUpdate')[0].files[0] != null) {
        $.ajax({
            url: "user/savePhoto",
            type: "post",
            processData: false,
            contentType: false,
            data: form,
            success: function (result) {
                if (result.code == 200 && result.data == true) {
                    alert("信息修改成功")
                    location.href = "client.html";
                }
                else if (result.code == 401) {
                    location.href = "login.html";
                }
                else if (result.code != 200){
                    alert(result.error+'头像修改失败');
                    location.href = "client.html";
                }
            }
        })

    }
    if ($("#password").val() == "" && $("#userName").val() == "") {
        return;
    }
    //保存用户名和密码
    $.ajax({
        url: '/user/saveUserInfo',
        type: 'post',
        data: {
            password: $("#password").val(),
            userName: $("#userName").val()
        },
        success: function (result) {
            if (result.code == 200 && result.data == true) {
                alert("信息修改成功")
                location.href = "client.html";
            }
            else if (result.code == 401) {
                location.href = "login.html";
            }
            else if (result.code != 200){
                alert(result.error);
                location.href = "client.html";
            }
        }
    });
}