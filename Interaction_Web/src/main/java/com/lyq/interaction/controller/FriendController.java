package com.lyq.interaction.controller;

import com.lyq.interaction.model.Friend;
import com.lyq.interaction.service.FriendService;
import com.lyq.interaction.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;

    /**
     * 获取好友列表
     * @return 返回好友列表
     */
    @RequestMapping("/getList")
    public List<Friend> getList(HttpSession session) {
        log.info("获取好友列表");
        String token = (String) session.getAttribute("userToken");
        Claims claims = JWTUtil.checkToken(token);

        return friendService.getFriendList((Integer) claims.get("userId"));
    }

    /**
     * 申请添加好友
     */
    @RequestMapping("/requestAdd")
    public Object requestAdd(Integer friendId, String message, HttpSession session) {
        log.info("申请添加好友 friendId: {}, message: {}",  friendId, message);
        String token = (String) session.getAttribute("userToken");
        Claims claims = JWTUtil.checkToken(token);
        Integer userId = (Integer) claims.get("userId");
        String userName = (String) claims.get("userName");
        return friendService.requestFriendAdd(friendId, userId, userName, message);
    }

    /**
     * 获取好友请求
     */
    @RequestMapping("/getRequestFriends")
    public Object getRequestFriends(HttpSession session) {
        log.info("获取好友请求");
        String token = (String) session.getAttribute("userToken");
        Claims claims = JWTUtil.checkToken(token);
        Integer userId = (Integer) claims.get("userId");
        return friendService.getWillFriend(userId);
    }

    /**
     * 接受好友请求
     * 操作成功返回true
     */
    @RequestMapping("/requestTrue")
    public Object requestTrue(Integer friendId, HttpSession session) {
        log.info("接受好友请求");
        String token = (String) session.getAttribute("userToken");
        Claims claims = JWTUtil.checkToken(token);
        Integer userId = (Integer) claims.get("userId");
        String userName = (String) claims.get("userName");
        return friendService.requestTrue(friendId, userId, userName);
    }

    /**
     * 拒绝好友申请
     */
    @RequestMapping("/requestFalse")
    public Object requestFalse(Integer friendId, HttpSession session) {
        log.info("拒绝好友申请");
        String token = (String) session.getAttribute("userToken");
        Claims claims = JWTUtil.checkToken(token);
        Integer userId = (Integer) claims.get("userId");
        return friendService.requestFalse(friendId, userId);
    }
}
