package com.lyq.interaction.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class LoginConfig implements WebMvcConfigurer {
    @Resource
    private LoginInterceptor loginInterceptor;

    @Value("${saveSessionPhotoPath}")
    private String saveSessionPhotoPath; //保存聊天图片的路径

    @Value("${photoPath}")
    private String photoPath;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/register")
                .excludePathPatterns("/**/**.html")
                .excludePathPatterns("/pic/**")
                .excludePathPatterns("/**/**.js")
                .excludePathPatterns("/**/**.css")
                .excludePathPatterns("/photo/**")
                .excludePathPatterns("/user/getUserByToken");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/photo/**", "/messagePhoto/**")
                .addResourceLocations("file:"+photoPath, "file:"+saveSessionPhotoPath);
    }
}
