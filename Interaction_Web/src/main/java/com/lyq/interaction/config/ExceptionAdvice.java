package com.lyq.interaction.config;

import com.lyq.interaction.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@Slf4j
@ControllerAdvice
@ResponseBody
public class ExceptionAdvice {
    @ExceptionHandler
    public Object Exception(Throwable e) {
        log.error("服务端发生异常 e:", e);
        return Result.fail("服务端发生异常");
    }
    @ExceptionHandler
    public Object Exception(MaxUploadSizeExceededException e) {
        log.error("文件过大 e:", e);
        return Result.fail("文件过大");
    }
}
