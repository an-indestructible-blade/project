package com.lyq.interaction.mapper;

import com.lyq.interaction.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface UserMapper {
    /**
     * 根据用户名获取密码
     * @param userName
     * @return
     */
    @Select("select * from user where delete_flag=1 and user_name=#{userName};")
    User getUserByName(String userName);

    /**
     * 创建用户
     * @param userName
     * @param password
     * @return
     */
    @Insert("insert into user(user_name, password) values (#{userName}, #{password});")
    Integer creatUser(String userName, String password);

    /**
     * 根据用户id获取用户名
     * @param userId
     * @return
     */
    @Select("SELECT user_name FROM user where user_id=#{userId} AND delete_flag = 1;")
    String getNameById(Integer userId);

    /**
     * 根据字符串模糊匹配用户名
     */
    @Select("SELECT user_id, user_name FROM user WHERE user_name LIKE concat('%',#{str},'%') AND delete_flag = 1;")
    List<User> getUserListByName(String str);

    /**
     * @param userId 用户id
     * @return 用户头像路径
     */
    @Select("SELECT photo FROM `user` WHERE user_id = #{userId};")
    String getUserPhoto(Integer userId);

    /**
     * @param userId 用户id
     * @param path 图片存储路径
     */
    @Update("UPDATE `user` SET photo = #{path} WHERE user_id = #{userId};")
    int addPhoto(Integer userId, String path);

    /**
     * @param userId 用户id
     * @param password 待修改的新密码
     */
    @Update("UPDATE `user` SET `password`=#{password} WHERE user_id=#{userId};")
    int setPassword(int userId, String password);

    /**
     * @param id 用户id
     * @param name 新的用户名
     */
    @Update("UPDATE `user` SET `user_name`=#{name} WHERE user_id=#{id};")
    int setUserName(int id, String name);
}
