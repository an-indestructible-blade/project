package com.lyq.interaction.mapper;

import com.lyq.interaction.model.SessionMassage;
import com.lyq.interaction.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SessionMassageMapper {
    //获取当前用户的所有sessionId并按照降序排列
    @Select("select id from session_massage where id in " +
            "(select session_id from user_session where user_id=#{userId}) order by last_time desc")
    List<Integer> getSessionIdByUserId(Integer userId);

    //根据sessionId获取对应的好友id和name（排除用户自己）
    @Select("select user_id, user_name from user where user_id in " +
            "(select user_id from user_session where session_id=#{sessionId} and user_id != #{userSelfId})")
    List<User> getSessionBySessionId(Integer sessionId, Integer userSelfId);

    //创建新会话并返回会话id
    Integer insertSession(SessionMassage sessionMassage);

    //创建新会话与用户的关系
    @Insert("INSERT INTO user_session VALUES (null, #{userId}, #{sessionId})")
    Integer insertUserFriend(Integer userId, Integer sessionId);
}
