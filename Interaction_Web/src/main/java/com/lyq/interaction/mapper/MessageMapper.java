package com.lyq.interaction.mapper;

import com.lyq.interaction.constant.MessageType;
import com.lyq.interaction.model.Message;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MessageMapper {
    //根据会话Id获取最新的一条消息
    @Select("SELECT * FROM message where session_id=#{id} ORDER BY create_time DESC LIMIT 1;")
    Message getNewMessage(Integer id);

    //根据会话id和start来获取10条消息
    @Select("SELECT * FROM message where session_id=#{sessionId} ORDER BY create_time DESC LIMIT #{start}, 10;")
    List<Message> getHistoryMessage(Integer sessionId, Integer start);

    //发送消息
    @Insert("INSERT INTO message VALUE (NULL, #{sessionId}, #{userId},  #{typeCode}, #{content},NOW());")
    Integer sendMessage(Integer userId, Integer sessionId, String content, Integer typeCode);
}
