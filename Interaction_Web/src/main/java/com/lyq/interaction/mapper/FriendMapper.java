package com.lyq.interaction.mapper;

import com.lyq.interaction.model.Friend;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FriendMapper {
    /**
     * 根据用户id获取好友列表
     */
    @Select("select friend_id, friend_name from friend where user_id=#{userId} AND delete_flag=1;")
    List<Friend> getFriendList(Integer userId);

    /**
     * 判断用户之间的关系
     */
    @Select("SELECT COUNT(*) FROM friend WHERE user_id=#{userId} AND friend_id=#{friendId} AND delete_flag=1;")
    int isFriend(Integer userId, Integer friendId);

    /**
     * 添加好友
     */
    @Insert("INSERT INTO friend VALUES " +
            "(null, #{userId}, #{friendId}, #{friendName}, 1)," +
            "(null, #{friendId}, #{userId}, #{userName}, 1);")
    Integer addFriend(Integer userId, String userName, Integer friendId, String friendName);
}
