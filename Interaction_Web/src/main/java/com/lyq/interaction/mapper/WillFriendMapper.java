package com.lyq.interaction.mapper;

import com.lyq.interaction.model.WillFriend;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface WillFriendMapper {
    /**
     * 添加好友申请
     */
    @Insert("INSERT INTO will_friend VALUES(NULL, #{sendId}, #{sendName}, #{receiveId}, #{message}, 1);")
    Integer addRequest(Integer receiveId, Integer sendId, String sendName, String message);

    /**
     * 根据用户和好友id查找申请记录
     */
    @Select("SELECT COUNT(*) FROM will_friend WHERE send_id=#{sendId} AND receive_id=#{receiveId} AND delete_flag = 1;")
    Integer findByFriendIdAndUserId(Integer sendId, Integer receiveId);

    /**
     * 获取指定用户的好友请求
     */
    @Select("SELECT * FROM will_friend WHERE receive_id=#{userId} AND delete_flag = 1;")
    List<WillFriend> getList(Integer userId);

    /**
     * 根据用户和好友id查找问候内容
     */
    @Select("SELECT message FROM will_friend WHERE send_id=#{sendId} AND receive_id=#{receiveId} AND delete_flag = 1;")
    String contentByFriendIdAndUserId(Integer sendId, Integer receiveId);

    /**
     * 删除好友申请信息
     */
    @Update("UPDATE will_friend SET delete_flag = 0 WHERE send_id=#{sendId} AND receive_id=#{receiveId} AND delete_flag = 1;")
    Integer deleteInfo(Integer sendId, Integer receiveId);
}
