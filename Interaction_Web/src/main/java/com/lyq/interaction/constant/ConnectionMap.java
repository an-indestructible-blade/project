package com.lyq.interaction.constant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Slf4j
public class ConnectionMap {
    private static final ConcurrentMap<Integer, WebSocketSession> map = new ConcurrentHashMap<>();

    public static WebSocketSession getWSSessionById(Integer userId) {
        return map.get(userId);
    }

    public static void onLine(Integer userId, WebSocketSession session) {
        map.put(userId, session);
        log.info("用户上线：{}", userId);
    }

    public static void offLine(Integer userId) {
        map.remove(userId);
        log.info("用户下线：{}", userId);
    }
}
