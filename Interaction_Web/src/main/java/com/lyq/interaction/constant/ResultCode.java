package com.lyq.interaction.constant;

import lombok.Getter;

public enum ResultCode {
    SUCCESS(200, "success"),
    FAIL(-1, "fail"),
    NOT_AUTHORITY(401, "notAuthority");

    private String desc;
    @Getter
    private Integer code;

    ResultCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
