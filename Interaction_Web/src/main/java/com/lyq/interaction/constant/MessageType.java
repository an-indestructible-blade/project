package com.lyq.interaction.constant;

import lombok.Getter;

public enum MessageType {
    TEXT(0, "文本"),
    PHONE(1, "图片");

    @Getter
    private int code;
    @Getter
    private String desc;

    private MessageType(int id, String name) {
        code = id;
        desc = name;
    }

}
