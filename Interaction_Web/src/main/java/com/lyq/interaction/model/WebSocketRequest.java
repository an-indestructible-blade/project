package com.lyq.interaction.model;

import com.lyq.interaction.constant.MessageType;
import lombok.Data;

@Data
public class WebSocketRequest {
    private MessageType type;
    private Integer sessionId;//会话Id
    private String content;

    public Integer getType() {
        return type.getCode();
    }

    public void setType(Integer code) {
        type = MessageType.values()[code];
    }
}
