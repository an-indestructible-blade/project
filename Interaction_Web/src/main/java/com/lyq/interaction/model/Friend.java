package com.lyq.interaction.model;

import lombok.Data;

//好友
@Data
public class Friend {
    private Integer friendId;
    private String friendName;
}
