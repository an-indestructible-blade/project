package com.lyq.interaction.model;

import com.lyq.interaction.constant.MessageType;
import lombok.Data;

import java.util.List;

//会话
@Data
public class SessionMessage {
    private Integer sessionId;
    private String name;
    private List<User> friend;
    private MessageType type;
    private String lastMessage;

    public Integer getType() {
        return type == null ? -1 : type.getCode();
    }

    public void setType(Integer code) {
        type = MessageType.values()[code];
    }
}
