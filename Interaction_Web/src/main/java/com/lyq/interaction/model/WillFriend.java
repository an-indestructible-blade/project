package com.lyq.interaction.model;

import lombok.Data;

@Data
public class WillFriend {
    private Integer id;
    private Integer sendId;
    private String sendName;
    private Integer receiveId;
    private String message;
}
