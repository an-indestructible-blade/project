package com.lyq.interaction.model;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    Integer userId;
    String userName;
    String password;
    String photo;
    Integer deleteFlag; //'0：删除 1：正常',
    Date createTime;
    Date updateTime;
}
