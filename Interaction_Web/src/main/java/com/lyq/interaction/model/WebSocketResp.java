package com.lyq.interaction.model;

import com.lyq.interaction.constant.MessageType;
import lombok.Data;

@Data
public class WebSocketResp {
    private MessageType type;
    private Integer sessionId;
    private Integer userId;//发送者id
    private String userName;//发送者的name
    private Object content;
    private String error;//错误信息

    public Integer getType() {
        return type.getCode();
    }

    public void setType(Integer code) {
        type = MessageType.values()[code];
    }
}
