package com.lyq.interaction.model;

import com.lyq.interaction.constant.MessageType;
import com.lyq.interaction.utils.DateUtil;
import lombok.Data;
import lombok.Getter;

import java.util.Date;

@Data
public class Message {
    private Integer id;
    private Integer sessionId;
    private Integer userId; //发送者
    private String userName;
    private MessageType type;
    private String content;
    private Date createTime;

    public String getCreateTime() {
        return DateUtil.Turn(createTime);
    }

    public Integer getType() {
        return type.getCode();
    }

    public void setType(Integer code) {
        type = MessageType.values()[code];
    }

    public String getTypeDesc() {
        return type.getDesc();
    }
}
