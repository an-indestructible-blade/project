package com.lyq.interaction.model;

import com.lyq.interaction.constant.ResultCode;

public class Result<T> {
    public ResultCode code; //200：成功   -1：发生异常  401：未登录
    public String error;
    public T data;

    public Integer getCode() {
        return code.getCode();
    }

    /**
     * 后端顺利执行完毕
     * @param data
     * @return
     * @param <T>
     */
    public static<T> Result<T> success(T data) {
        Result<T> ret = new Result<>();
        ret.data = data;
        ret.code = ResultCode.SUCCESS;
        return ret;
    }

    /**
     * 后端在处理请求时发生异常
     * @param error
     * @return
     * @param <T>
     */
    public static<T> Result<T> fail(String error) {
        Result<T> ret = new Result<>();
        ret.code = ResultCode.FAIL;
        ret.error = error;
        return ret;
    }

    /**
     * 该用户权限不足（未登录）
     * @return
     * @param <T>
     */
    public static<T> Result<T> notAuthority() {
        Result<T> ret = new Result<>();
        ret.code = ResultCode.NOT_AUTHORITY;
        return ret;
    }
}
