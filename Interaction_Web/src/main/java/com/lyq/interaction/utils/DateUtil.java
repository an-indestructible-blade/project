package com.lyq.interaction.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static String Turn(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return simpleDateFormat.format(date);
    }
}
