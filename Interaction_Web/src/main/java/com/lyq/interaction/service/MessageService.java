package com.lyq.interaction.service;

import com.lyq.interaction.mapper.MessageMapper;
import com.lyq.interaction.mapper.UserMapper;
import com.lyq.interaction.model.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private MessageMapper messageMapper;
    //根据会话id和start来获取10条消息
    public Object getHistoryMessage(Integer sessionId, Integer start) {
        List<Message> list = new ArrayList<>();
        list = messageMapper.getHistoryMessage(sessionId, start);
        Iterable<Message> iterable = new ArrayList<>(list);
        for (Message message : iterable) {
            message.setUserName(userMapper.getNameById(message.getUserId()));
        }
        return list;
    }
}
