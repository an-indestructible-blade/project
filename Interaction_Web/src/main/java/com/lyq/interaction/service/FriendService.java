package com.lyq.interaction.service;

import com.lyq.interaction.constant.MessageType;
import com.lyq.interaction.mapper.FriendMapper;
import com.lyq.interaction.mapper.MessageMapper;
import com.lyq.interaction.mapper.UserMapper;
import com.lyq.interaction.mapper.WillFriendMapper;
import com.lyq.interaction.model.Friend;
import com.lyq.interaction.model.Result;
import com.lyq.interaction.model.SessionMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FriendService {
    @Autowired
    private FriendMapper friendMapper;
    @Resource
    private WillFriendMapper willFriendMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private SessionService sessionService;
    @Resource
    private MessageMapper messageMapper;
    /**
     * 获取好友列表
     */
    public List<Friend> getFriendList(Integer userId) {
        List<Friend> ret = friendMapper.getFriendList(userId);
        return ret;
    }

    /**
     * 申请添加好友
     */
    public Object requestFriendAdd(Integer friendId, Integer userId, String userName, String message) {
        //查看该申请是否已经存在
        if (willFriendMapper.findByFriendIdAndUserId(userId, friendId) != 0) {
            return Result.fail("请勿重复操作");
        }
        if (!StringUtils.hasLength(message)) {
            message = "你好";
        }
        return 1 == willFriendMapper.addRequest(friendId, userId, userName, message);
    }

    //获取指定用户的好友请求
    public Object getWillFriend(Integer userId) {
        return willFriendMapper.getList(userId);
    }

    //接受好友申请
    @Transactional
    public Object requestTrue(Integer friendId, Integer userId, String userName) {
        //获取好友名字
        String friendName = userMapper.getNameById(friendId);
        if (!StringUtils.hasLength(friendName)) {
            Result.fail("该用户已不存在");
        }
        //添加好友
        if (2 != friendMapper.addFriend(friendId, friendName, userId, userName)) {
            throw new RuntimeException("好友插入异常");
        }
        //创建会话
        List<Integer> list = new ArrayList<>();
        list.add(friendId);
        list.add(userId);
        SessionMessage sessionMessage = (SessionMessage) sessionService.createSession(list);
        String message = willFriendMapper.contentByFriendIdAndUserId(friendId, userId);
        //添加打招呼信息
        Integer a = messageMapper.sendMessage(friendId, sessionMessage.getSessionId(), message, MessageType.TEXT.getCode());
        if (a != 1) {
            throw new RuntimeException("消息存储异常");
        }
        //删除好友申请消息
        return 1 == willFriendMapper.deleteInfo(friendId, userId);
    }

    /**
     * 拒绝好友申请
     */
    public Object requestFalse(Integer friendId, Integer userId) {
        //删除好友申请消息
        return 1 == willFriendMapper.deleteInfo(friendId, userId);
    }
}
