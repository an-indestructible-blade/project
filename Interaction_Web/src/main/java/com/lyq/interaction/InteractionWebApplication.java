package com.lyq.interaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class InteractionWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(InteractionWebApplication.class, args);
    }

}
/**
 * 登录接口已完成、注册接口完成
 * Token创建完成，完成解析
 * MD5加密工具完成
 * 统一异常处理、统一功能返回已完成
 * 前端主页面完成
 * 添加会话完成
 * 获取历史消息
 * 搜索用户
 * 加好友
 * 建群
 * 头像
 */
