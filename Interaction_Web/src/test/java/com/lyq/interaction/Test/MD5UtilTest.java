package com.lyq.interaction.Test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
public class MD5UtilTest {
    @Test
    public void puzzle() {
        String pas = "zhaoliu";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String md5 = DigestUtils.md5DigestAsHex((pas+uuid).getBytes(StandardCharsets.UTF_8));
        System.out.println(md5 + uuid);
    }
    @Test
    public void verify() {
        String puzzleMd5 = "7b81eb553606b5aa431839c9877ec59a1b3a467320984667a41b67a3e33ab020";
        String password = "zhangsan";
        String uuid = puzzleMd5.substring(32, 64);
        String md5 = puzzleMd5.substring(0, 32);
        String tmp = DigestUtils.md5DigestAsHex((password+uuid).getBytes(StandardCharsets.UTF_8));
        if (md5.equals(tmp)) {
            log.info("密码正确");
        }
        else {
            log.info("密码错误");
        }
    }
}
