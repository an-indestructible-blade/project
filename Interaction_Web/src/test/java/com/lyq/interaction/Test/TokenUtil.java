package com.lyq.interaction.Test;

import io.jsonwebtoken.JwtParserBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class TokenUtil {
    private static final long DURING_TIME = 1000*60*60*24;
    private static final String STR = "pphFphDj5GK12AE9R7/y723Cx7hujdhDy/eDV7MLM4=";
    private static final Key key = Keys.hmacShaKeyFor(STR.getBytes(StandardCharsets.UTF_8));
//    private static final Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(STR));

    /**
     * 获取用户Token
     */
    @Test
    public void getToken() {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", 1);
        map.put("userName", "zhangsan");

        String token = Jwts.builder()
                .addClaims(map)
                .setExpiration(new Date(System.currentTimeMillis() + DURING_TIME))
                .signWith(key)
                .compact();
        System.out.println(token);
    }

    @Test
    public void checkToken() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InpoYW5nc2FuIiwidXNlcklkIjoxLCJleHAiOjE3MTEwMDA5OTJ9.dWMHUp1eRhVbaEK2DAVGoJm89YhRYaB_6qY941kQ4E4";
        System.out.println(Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody());
    }

    /**
     * 生成密钥
     */
    @Test
    public void creat() {
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        System.out.println(Encoders.BASE64.encode(key.getEncoded()));
    }
}
