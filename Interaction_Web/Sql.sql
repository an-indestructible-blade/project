create database if not exists web_interaction charset utf8;

use web_interaction;

drop table if exists user;
-- 用户表
create table user(
    user_id int(32) primary key AUTO_INCREMENT,
    user_name varchar(20) unique NOT NULL ,
    password varchar(64)  NOT NULL ,
    photo varchar(255) default "photo/0.png",
    delete_flag tinyint default 1 COMMENT '0：删除 1：正常',
    create_time DATETIME default now(),
    update_time DATETIME default now()
);

-- 真实密码和账号相同，数据库中password存的是加密后的密文
insert into user(user_name, password) values
    ('lisi', '35f9a51ba953f1afcc44fec1fd8aba7326871a7133ea4d09927190e02241a022'),
    ('zhangsan', '7b81eb553606b5aa431839c9877ec59a1b3a467320984667a41b67a3e33ab020'),
    ('zhaoliu', 'e60b2a541d686f1a91bf068ecb55e1cec7a1734fe8d044d2ad288f026a087e46'),
    ('lyq', 'a0135e4ad7b95b58502eb60e222d05845d2b98a68be544468ba97bc0a9d6cf19');

drop table if exists friend;

-- 好友表
create table friend(
    id int(32) primary key AUTO_INCREMENT,
    user_id int(32),
    friend_id int(32),
    friend_name varchar(20),
    delete_flag tinyint default 1 COMMENT '0：删除 1：正常'
);

insert into friend values
    (null, 1, 2, 'zhangsan', 1),
    (null, 2, 1, 'lisi', 1),
    (null, 1, 3, 'zhaoliu', 1),
    (null, 3, 1, 'lisi', 1),
    (null, 3, 2, 'zhangsan', 1),
    (null, 2, 3, 'zhaoliu', 1);

drop table if exists session_message;
-- 会话表
create table session_message(
    id int(32) primary key AUTO_INCREMENT,
    name varchar(255),
    last_time DATETIME default now()
);

insert into session_message(id, name) values
                                            (1, '家和万事兴'),
                                            (2, '万事如意');


drop table if exists user_session;
-- 用户和会话关系表
create table user_session(
    id int(32) primary key AUTO_INCREMENT,
    user_id int(32),
    session_id int(32)
);
insert into user_session values
                            (null, 1, 1),
                            (null, 2, 1),
                            (null, 1, 2),
                            (null, 3, 2);

drop table if exists message;
-- 消息表
create table message(
    id int(32) primary key AUTO_INCREMENT,
    session_id int(32) COMMENT '该条消息属于那个会话',
    user_id int(32) COMMENT '该条消息的发送人',
    type int(2) COMMENT '消息类型：0、文本；1、图片',
    content text,
    create_time DATETIME
);

insert into message values(null, 1, 1, 0, '作业写了没？', '2000-05-01 14:59:00');
insert into message values(null, 1, 1, 0, '借我抄一下', '2000-05-01 16:58:00');
insert into message values(null, 1, 1, 0, '在吗？', '2000-05-01 16:59:00');
insert into message values(null, 1, 1, 0, '在吗？', '2000-05-01 16:59:01');
insert into message values(null, 1, 1, 0, '今晚吃啥', '2000-05-01 17:00:00');
insert into message values(null, 1, 2, 0, '随便', '2000-05-01 17:01:00');
insert into message values(null, 1, 1, 0, '那吃面?', '2000-05-01 17:02:00');
insert into message values
    (null, 1, 2, 0, '不想吃', '2000-05-01 17:03:00');
insert into message values
    (null, 1, 1, 0, '那你想吃啥', '2000-05-01 17:04:00');
insert into message values
    (null, 1, 2, 0, '随便', '2000-05-01 17:05:00');
insert into message values
    (null, 1, 1, 0, '那吃米饭炒菜?', '2000-05-01 17:06:00');
insert into message values
    (null, 1, 2, 0, '不想吃', '2000-05-01 17:07:00');
insert into message values
    (null, 1, 1, 0, '那你想吃啥?', '2000-05-01 17:08:00');
insert into message values
    (null, 1, 2, 0, '随便', '2000-05-01 17:09:00');
insert into message values
    (null, 2, 1, 0, '晚上一起约?', '2000-05-02 12:00:00');

drop table if exists will_friend;

-- 好友申请表
create table will_friend(
    id INT(32) primary key AUTO_INCREMENT,
    send_id INT(32) NOT NULL ,
    send_name varchar(20) NOT NULL ,
    receive_id INT(32) NOT NULL ,
    message TEXT,
    delete_flag tinyint default 1 COMMENT '0：删除 1：正常'
);

-- insert into will_friend values (null, 4, 'lyq', 2, '你好', 1);