package operation;

import book.BookList;

public class ExitOperation extends Work{

    @Override
    public void work(BookList bookList) {
        System.exit(0);
    }
}
