package operation;

import book.BookList;

public class ShowOperation extends Work{

    @Override
    public void work(BookList bookList) {
        for (int i = 0; i < bookList.numBooks; i++) {
            System.out.println(bookList.books[i]);
        }
    }
}
