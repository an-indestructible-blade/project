package operation;

import book.BookList;

abstract public class Work {
    abstract public void work(BookList bookList);
}
