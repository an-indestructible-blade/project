package operation;

import book.BookList;

import java.util.Scanner;

public class BorrowOperation extends Work{

    @Override
    public void work(BookList bookList) {
        int numBooks = bookList.numBooks;

        Scanner in = new Scanner(System.in);
        System.out.print("请输入书名：");
        String name = in.nextLine();

        for (int i = 0; i < numBooks; i++) {
            if (name.equals(bookList.books[i].getName())) {
                if (bookList.books[i].getBorrow() == false){
                    System.out.println("借阅成功");
                    bookList.books[i].setBorrow(true);
                    return;
                }else{
                    System.out.println("该书已被借出");
                    return;
                }
            }
        }
        System.out.println("未收录该书");
    }
}
