package operation;

import book.BookList;

import java.util.Scanner;

public class FindOperation extends Work{

    @Override
    public void work(BookList bookList) {
        int numBooks = bookList.numBooks;

        Scanner in = new Scanner(System.in);
        System.out.print("请输入书名：");
        String name = in.nextLine();

        for (int i = 0; i < numBooks; i++) {
            if (name.equals(bookList.books[i].getName())) {
                System.out.println(bookList.books[i]);
                return;
            }
        }
        System.out.println("未收录该书");
    }
}
