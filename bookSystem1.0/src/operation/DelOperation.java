package operation;

import book.BookList;

import java.util.Scanner;

public class DelOperation extends Work{

    @Override
    public void work(BookList bookList) {
        int numBooks = bookList.numBooks;

        Scanner in = new Scanner(System.in);
        System.out.print("请输入书名：");
        String name = in.nextLine();

        for (int i = 0; i < numBooks; i++) {
            if (name.equals(bookList.books[i].getName())){
                int j = 0;
                for (j = i; j < numBooks-1; j++) {
                    bookList.books[j] = bookList.books[j + 1];
                }
                bookList.books[j] = null;
                bookList.numBooks--;
                return;
            }
        }
    }
}
