package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

public class AddOperation extends Work{

    @Override
    public void work(BookList bookList) {
        Scanner in = new Scanner(System.in);
        bookList.books[bookList.numBooks] = new Book();
        System.out.print("请输入书名：");
        bookList.books[bookList.numBooks].setName(in.nextLine());
        System.out.print("请输入作者：");
        bookList.books[bookList.numBooks].setAuthor(in.nextLine());
        System.out.print("请输入所属书类：");
        bookList.books[bookList.numBooks].setType(in.nextLine());
        System.out.print("请输入书价：");
        bookList.books[bookList.numBooks].setMoney(in.nextInt());
        bookList.numBooks++;
    }
}
