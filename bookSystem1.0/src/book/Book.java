package book;

public class Book {
    private String name;
    private String author;
    private int money;
    private String type;
    private boolean isBorrow;

    public Book(){};
    public Book(String name, String author, int money, String kind) {
        this.name = name;
        this.author = author;
        this.money = money;
        this.type = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String kind) {
        this.type = kind;
    }

    public boolean getBorrow() {
        return isBorrow;
    }

    public void setBorrow(boolean borrow) {
        isBorrow = borrow;
    }

    @Override
    public String toString() {
        return  "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", money=" + money +
                ", kind='" + type + '\'' +
                ", 状态："+(isBorrow == false ? "未被借出" : "已被借出");
    }
}
