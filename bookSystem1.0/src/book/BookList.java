package book;

public class BookList {
    public Book[] books;//书架
    public int numBooks;//书架实际存放的书的本书

    public BookList() {
        this.books = new Book[10];
        //默认书架存放两本书
        this.books[0] = new Book("西游记","吴承恩",39,"小说");
        this.books[1] = new Book("红楼梦","曹雪芹",39,"小说");
        this.numBooks = 2;
    }


}
