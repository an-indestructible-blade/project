package user;

import book.BookList;
import operation.Work;

abstract public class User {
    protected String name;
    protected Work[] work;//存放用户所有的操作
    public int counst; //work中的元素个数
    abstract public int menu();

    public void operation(int choice, BookList bookList){
        work[choice].work(bookList);
    }
}
