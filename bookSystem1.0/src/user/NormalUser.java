package user;

import operation.*;

import java.util.Scanner;

public class NormalUser extends User {

    public NormalUser(String name){
        this.name = name;
        this.work = new Work[]{
                new ExitOperation(),
                new FindOperation(),
                new BorrowOperation(),
                new RetOperation(),
        };
        this.counst = 4;
    }
    @Override
    public int menu() {
        System.out.println("###########################");
        System.out.println(" Hello " + "NormalUser：" + this.name);
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("###########################");
        System.out.println("请选择你的操作：");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }
}
