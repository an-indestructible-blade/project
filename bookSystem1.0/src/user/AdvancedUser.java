package user;

import operation.*;

import java.util.Scanner;

public class AdvancedUser extends User{

    public AdvancedUser(String name){
        this.name = name;
        this.work = new Work[]{
                new ExitOperation(),
                new FindOperation(),
                new AddOperation(),
                new DelOperation(),
                new ShowOperation()
        };
        this.counst = 5;
    }

    @Override
    public int menu() {
        System.out.println("###########################");
        System.out.println(" Hello " + "AdvancedUser：" + this.name);
        System.out.println("1.查找图书");
        System.out.println("2.新增图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("###########################");
        System.out.println("请选择你的操作：");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }
}
