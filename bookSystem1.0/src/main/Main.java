package main;

import book.BookList;
import user.AdvancedUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

public class Main {

    public static User login(){
        Scanner in = new Scanner(System.in);
        System.out.println("请输入你的姓名：");
        String name = in.nextLine();
        System.out.println("请选择你的身份：1.管理员    0.普通用户");
        if (in.nextInt() == 1){
            return new AdvancedUser(name);
        }else {
            return new NormalUser(name);
        }
    }

    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();
        while(true){
            int choice;
            do{
                choice = user.menu();
            }while(choice < 0 || choice >= user.counst);

            user.operation(choice, bookList);
        }
    }
}
