package com.example.calculator;

import java.util.ArrayList;
import java.util.Stack;

public class MidChangeEnd {

    public ArrayList<String> midChangeEng(String str) {
        //这里用动态数组就不用担心不够用
        ArrayList<String> ret = new ArrayList<String>();
        Stack<Character> stack = new Stack<>();

        int[] able = {1,0,0,0,0,1};//分别代表 * + (null) - (null) / 的优先级
        //遍历字符串
        for (int i = 0; i < str.length(); i++) {
            char a = str.charAt(i);

            if (isOperator(a)) {
                //是操作数
                switch (a) {
                    case '+'://判断如果优先级不大于栈顶的元素那么就先出栈在入栈
                        if(!stack.isEmpty()) {//出栈
                            while(!stack.isEmpty() && stack.peek() != '(' && able[(int)stack.peek() - 42] >= able[(int)a-42]) {
                                String b = "";
                                b += stack.pop();
                                ret.add(b);
                            }
                        }
                        stack.push(a);
                        break;
                    case '-':
                        if(!stack.isEmpty()) {//出栈
                            while(!stack.isEmpty() && stack.peek() != '(' && able[(int)stack.peek() - 42] >= able[(int)a-42]) {
                                String b = "";
                                b += stack.pop();
                                ret.add(b);
                            }
                        }
                        stack.push(a);
                        break;
                    case '*':
                        if(!stack.isEmpty()) {//出栈
                            while(!stack.isEmpty() && stack.peek() != '(' && able[(int)stack.peek() - 42] >= able[(int)a-42]) {
                                String b = "";
                                b += stack.pop();
                                ret.add(b);
                            }
                        }
                        stack.push(a);
                        break;
                    case '/':
                        if(!stack.isEmpty()) {//出栈
                            while(!stack.isEmpty() && stack.peek() != '(' && able[(int)stack.peek() - 42] >= able[(int)a-42]) {
                                String b = "";
                                b += stack.pop();
                                ret.add(b);
                            }
                        }
                        stack.push(a);
                        break;
                    case '(':
                        stack.push(a);
                        break;
                    case ')':
                        while(!stack.isEmpty() && stack.peek() != '(') {
                            String b = "";
                            b += stack.pop();
                            ret.add(b);
                        }
                        stack.pop();//删除‘(’
                        break;
                }

            }else{
                //如果是数字就放到ret中
                String tmp = "";
                //数字有可能是多位的
                while(i < str.length() && !isOperator(str.charAt(i))) {
                    tmp += str.charAt(i);
                    i++;
                }
                i--;
                ret.add(tmp);
            }
        }
        //将栈里面剩余的全部出栈
        while(!stack.isEmpty()) {
            String b = "";
            b += stack.pop();
            ret.add(b);
        }
        return ret;
    }

    public boolean isOperator(char s) {
        if (s == '+' || s == '-' || s == '*' || s == '/' || s == '(' || s == ')') {
            return true;
        }
        return false;
    }
}
