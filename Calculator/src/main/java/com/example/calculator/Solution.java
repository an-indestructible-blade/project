package com.example.calculator;
 
public class Solution {
 
    public String Mul(String s, String t) {
        //计算s*t
        //判断特殊情况
        if (s.charAt(0) == '0' || t.charAt(0) == '0'){
            return "0";
        }
        //用来存储返回结果
        String ret = "0";
        //定义一个字符串数组 tmp 用来存储 t 中的每一位与 s 相乘的结果。
        String[] tmp = new String[t.length()]; 
        for (int i = t.length() - 1; i >= 0; i--){
            tmp[i] = "";
            int j = t.length() - 1;

            while(j - i > 0){
                tmp[i] += '0';
                j--;
            }
            //实现n位数与一位数相乘并返回字符串结果
            tmp[i] += alongMultiply(s, t.charAt(i));
        }
        //将字符串数组中的每一位结果相加并存储在ret中
        for (int i = t.length() - 1; i >= 0; i--){
            ret = add(tmp[i], ret);
        }

        //将结果逆置
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = ret.length() - 1; i >= 0; i--){
            stringBuffer.append(ret.charAt(i));
        }
        ret = stringBuffer.toString();

        return ret;
    }

    public String Add(String s, String t) {
        if (s.charAt(0) == '0'){
            return t;
        }
        if (t.charAt(0) == '0'){
            return s;
        }
        String ret = "";
        int sLen = s.length() - 1;
        int tLen = t.length() - 1;
        //存储进位
        int ten = 0;
        while (sLen >= 0 && tLen >= 0) {
            int tmp = (s.charAt(sLen--) - '0') + (t.charAt(tLen--) - '0');
            tmp += ten;
            ten = tmp / 10;
            ret += tmp % 10;
        }
        while(sLen >= 0){
            int tmp = s.charAt(sLen--) - '0';
            tmp += ten;
            ten = tmp / 10;
            ret += tmp % 10;
        }
        while(tLen >= 0){
            int tmp = t.charAt(tLen--) - '0';
            tmp += ten;
            ten = tmp / 10;
            ret += tmp % 10;
        }
        if (ten != 0){
            ret += ten;
        }

        //将结果逆置
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = ret.length() - 1; i >= 0; i--){
            stringBuffer.append(ret.charAt(i));
        }
        ret = stringBuffer.toString();

        return ret;
    }
 
    public String add(String a, String b){
        //用来存储返回结果
        String str = "";
        int aLen = a.length() - 1;
        int ai = 0;
        int bLen = b.length() - 1;
        int bi = 0;
        //存储进位
        int ten = 0;
 
        while(aLen >= ai && bLen >= bi){
            int tmp = (a.charAt(ai++) - '0') + (b.charAt(bi++) - '0');
            tmp += ten;
            ten = tmp / 10;
            str += tmp % 10;
        }

        while(aLen >= ai){
            int tmp = a.charAt(ai++) - '0';
            tmp += ten;
            ten = tmp / 10;
            str += tmp % 10;
        }
        while(bLen >= bi){
            int tmp = b.charAt(bi++) - '0';
            tmp += ten;
            ten = tmp / 10;
            str += tmp % 10;
        }
        if (ten != 0){
            str += ten;
        }
        return str;
    }
 
    public String alongMultiply(String s, char t){
        //用来存储返回结果
        String ret = "";
        if (s.charAt(0) == '0' || t == '0'){
            return "0";
        }
        int tt = t - '0';
        //存储进位
        int ten = 0;
        for (int i = s.length() - 1; i >= 0; i--){
            int tmp = s.charAt(i) - '0';
            tmp *= tt;
            tmp += ten;
            ten = tmp / 10;
            ret += tmp % 10;
        }
        if (ten != 0){
            ret += ten;
        }

        return ret;
    }
}