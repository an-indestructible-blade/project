/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : book_test

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/02/2024 14:36:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book_info
-- ----------------------------
DROP TABLE IF EXISTS `book_info`;
CREATE TABLE `book_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `author` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `count` int(11) NOT NULL,
  `price` decimal(7, 2) NOT NULL,
  `publish` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '1,可借阅 0,不可借阅',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_flag` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1-图书状态正常 0-图书已下架',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of book_info
-- ----------------------------
INSERT INTO `book_info` VALUES (1, '活\r\n着', '余华', 29, 22.00, '北京文艺出版社', 1, '2024-01-27 13:41:21', '2024-02-07 14:15:14', 0);
INSERT INTO `book_info` VALUES (2, '平凡的世界333333', '路遥', 5, 98.56, '北京十月文艺出版社', 1, '2024-01-27 13:41:21', '2024-02-08 13:25:04', 0);
INSERT INTO `book_info` VALUES (3, '三体ti', '刘慈欣', 111, 102.67, '重庆出版社', 1, '2024-01-27 13:41:21', '2024-02-08 13:25:12', 0);
INSERT INTO `book_info` VALUES (4, '⾦字塔\r\n原理', '⻨肯锡', 16, 178.00, '民主与建设出版社', 1, '2024-01-27 13:41:21', '2024-02-08 13:25:12', 0);
INSERT INTO `book_info` VALUES (5, 'qwe', 'qw', 23, 333.00, 'rrrrrr', 1, '2024-01-31 18:13:38', '2024-02-08 13:25:12', 0);
INSERT INTO `book_info` VALUES (40, '三体ti', '刘慈欣', 111, 102.67, '重庆出版社', 1, '2024-02-05 15:34:19', '2024-02-05 15:34:49', 0);
INSERT INTO `book_info` VALUES (41, '平凡的世界333333', '路遥', 5, 98.56, '北京十月文艺出版社', 1, '2024-02-08 13:25:27', '2024-02-08 13:25:27', 1);
INSERT INTO `book_info` VALUES (44, 'hhhhh', '零零零零', 34, 989.00, 'asdasd', 1, '2024-02-08 14:10:08', '2024-02-08 14:10:08', 1);

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_flag` tinyint(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1-账号状态正常 0-账号已注销',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (5, 'zhangsan', 'zhangsan', '2024-01-29 18:42:24', '2024-01-29 18:42:24', 1);

SET FOREIGN_KEY_CHECKS = 1;
