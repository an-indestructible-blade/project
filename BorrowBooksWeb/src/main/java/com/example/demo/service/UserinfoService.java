package com.example.demo.service;

import com.example.demo.mapper.UserinfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserinfoService {
    @Autowired
    private UserinfoMapper userinfoMapper;
    public boolean checkLogin(String name, String password){
        try{
            return userinfoMapper.selectPassword(name).getPassword().equals(password);
        } catch (NullPointerException e) {
            return false;
        }
    };
}
