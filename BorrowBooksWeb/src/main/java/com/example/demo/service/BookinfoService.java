package com.example.demo.service;

import com.example.demo.bookEnum.BookEnum;
import com.example.demo.mapper.BookinfoMapper;
import com.example.demo.model.Book;
import com.example.demo.model.PageRequest;
import com.example.demo.model.PageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookinfoService {
    @Autowired
    private BookinfoMapper bookinfoMapper;

    public PageResponse<Book> getBookList(PageRequest pageRequest) {
        List<Book> list = bookinfoMapper.getPageBook(pageRequest);
        Integer count = bookinfoMapper.getCount();
        for (Book book : list) {
            if (book.getStatus() == 1) {
                book.setStateCH(BookEnum.NORMAL.getData());
            } else {
                book.setStateCH(BookEnum.ANOMALY.getData());
            }
        }
        PageResponse<Book> pageResponse = new PageResponse<>();
        pageResponse.setBookList(list);
        pageResponse.setCount(count);
        pageResponse.setNowPage(pageRequest.getNowPage());
        return pageResponse;
    }

    public boolean deleteBook(Book book) {
        book.setDeleteFlag(0);
        if (bookinfoMapper.updata(book) == 1) {
            return true;
        }
        return false;
    }

    public Book getBookById(Integer id) {
        return bookinfoMapper.getBookById(id);
    }

    public boolean addBook(Book book) {
        if (bookinfoMapper.addBook(book) == 1) {
            return true;
        }
        return false;
    }

    public Boolean updateBook(Book book) {
        if (bookinfoMapper.updata(book) == 1) {
            return true;
        }
        return false;
    }

    public Boolean deleteBooks(List<Integer> ids) {
        if (bookinfoMapper.updataList(ids) == ids.size()) {
            return true;
        }
        else {
            throw new RuntimeException("批量删除发生异常");
        }
    }
}
