package com.example.demo.advice;

import com.example.demo.model.Resp;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptAdvice {
    @ExceptionHandler
    public Resp<String> eHandler(Throwable a) {
        return Resp.fail("系统内部发生异常");
    }
}
