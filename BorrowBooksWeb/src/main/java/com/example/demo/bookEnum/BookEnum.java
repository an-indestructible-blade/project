package com.example.demo.bookEnum;

public enum BookEnum {
    NORMAL("可借阅", 1),
    ANOMALY("不可借阅", 0);

    private String data;
    private Integer code;

    BookEnum(String data, Integer code) {
        this.data = data;
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public Integer getCode() {
        return code;
    }
}
