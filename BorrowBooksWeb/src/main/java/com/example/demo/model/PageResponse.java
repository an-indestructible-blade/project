package com.example.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class PageResponse<T> {
    //返回图书列表
    private List<T> BookList;
    //书籍总数
    private Integer count;
    //当前页码
    private Integer nowPage;
}
