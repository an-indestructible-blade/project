package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Book {
    private Integer id;
    private String bookName;
    private String author;
    private Integer count;
    private BigDecimal price;
    private String publish;
    private Integer status;//1,可借阅 0,不可借阅
    private String stateCH;
    private Date createTime;
    private Date updateTime;
    private Integer deleteFlag;
}
