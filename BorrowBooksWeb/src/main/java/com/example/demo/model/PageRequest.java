package com.example.demo.model;

import lombok.Data;

@Data
public class PageRequest {
    private Integer nowPage = 1;
    private Integer pageSize = 10;
    private Integer offset;

    public Integer getOffset() {
        return (nowPage - 1) * pageSize;
    }
}
