package com.example.demo.model;

import lombok.Data;

@Data
public class Resp<T> {
    //200-正常  0-发生异常
    private Integer code;
    //错误信息
    private String desc;
    //返回数据
    private T data;

    public static <T> Resp<T> fail(String str) {
        Resp<T> resp = new Resp<>();
        resp.setCode(0);
        resp.setDesc(str);
        return resp;
    }

    public static <T> Resp<T> seccess(T data) {
        Resp<T> resp = new Resp<>();
        resp.setCode(200);
        resp.setData(data);
        return resp;
    }
}
