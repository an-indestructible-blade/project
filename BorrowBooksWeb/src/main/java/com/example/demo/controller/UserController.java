package com.example.demo.controller;

import com.example.demo.constant.Constants;
import com.example.demo.service.UserinfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@Slf4j
@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserinfoService userinfoService;

    @RequestMapping("/login")
    public boolean login(String name, String password, HttpSession session) {
        log.info("登陆：name:"+name+" password:"+password);
        if (!StringUtils.hasLength(name) || !StringUtils.hasLength(password)) {
            return false;
        }
        if (userinfoService.checkLogin(name, password)) {
            session.setAttribute(Constants.SESSION_KEY, name);
            return true;
        }
        return false;
    }
}
