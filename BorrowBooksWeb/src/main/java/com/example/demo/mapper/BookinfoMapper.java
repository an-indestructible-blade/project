package com.example.demo.mapper;

import com.example.demo.model.Book;
import com.example.demo.model.PageRequest;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookinfoMapper {
    List<Book> getPageBook(PageRequest pageRequest);
    //获取图书总数
    Integer getCount();
    //更新图书信息
    Integer updata(Book book);
    //根据id查询图书
    Book getBookById(Integer id);
    //新增图书
    Integer addBook(Book book);

    Integer updataList(List<Integer> ids);
}
