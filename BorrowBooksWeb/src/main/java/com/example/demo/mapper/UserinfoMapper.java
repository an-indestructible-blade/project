package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserinfoMapper {
    //根据用户名查找数据
    User selectPassword(String name);
}
